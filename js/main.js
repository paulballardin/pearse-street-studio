$(function() {


	var $grid = $('.grid').masonry({
	  // set itemSelector so .grid-sizer is not used in layout
		columnWidth: '.grid-sizer',
		itemSelector: '.grid-item',
		percentPosition: true
	})


	// change size of item by toggling gigante class
	$('.grid-item').on( 'click', function() {
	  // trigger layout after item size changes

	  displayImages();
	});



	//Function to set up the array of available images
	var imageIds = [];
	
	var setArray = function(first,last ){
		for (i=first; i <= last; i++ ){
			imageIds.push(i);
		}
	}



	//Function to set up a random array of image ids
	var shuffle = function (array) {
	  var currentIndex = array.length, temporaryValue, randomIndex;

	  // While there remain elements to shuffle...
	  while (0 !== currentIndex) {

	    // Pick a remaining element...
	    randomIndex = Math.floor(Math.random() * currentIndex);
	    currentIndex -= 1;

	    // And swap it with the current element.
	    temporaryValue = array[currentIndex];
	    array[currentIndex] = array[randomIndex];
	    array[randomIndex] = temporaryValue;
	  }

	  return array;
	}	

	// This function displays the images
	var displayImages = function (){
		setArray(1,16);
		shuffle(imageIds);

		// Set up our variables
		var grid = $('.grid');

		$(grid).find('.grid-item').each(function(index, el) {
			imageId = imageIds[index];
			$(this).css('background-image', 'url(img/img-' + imageId + '.jpeg)');
		});
		imageIds = [];
	};


	displayImages();


	var cardSizes = function() {
		var aside = $('aside.sidebar');
		var brand = $('h1.brand');
		var profiles = $('.profile');
		var cardHeight = (aside.outerHeight(true) - brand.outerHeight(true)) / profiles.length;
		console.log(cardHeight);
		$(profiles).each(function(index, el) {
			$(this).css('height', cardHeight);
		});

		
	}

	
	var checkDesktop = function(){
    if ($(".main").css("display") !='none' ){
    	$('aside.sidebar').css('height', Math.max(window.innerHeight, 768));
		$('div.main').css('height',  Math.max(window.innerHeight, 768));
        cardSizes();
	 } else {
	 	$('.profile').each(function(index, el) {
			$(this).css('height', 'auto');
		});
	 }

	}
	checkDesktop();	
	
	window.onresize = resize;
	function resize() { 
	checkDesktop();	
	displayImages();
	}


})

